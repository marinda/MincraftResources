<?php
/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2020/8/10 0010
 * Time: 22:30
 */

require_once "mysqlinfo.php";
require_once "phpass-0.5/PasswordHash.php";

$param_username = $_GET['username'];
$param_pwd = $_GET['password'];

$con = mysqli_connect(MYSQL_HOST,MYSQL_USERNAME,MYSQL_PASSWORD,MYSQL_DATABASE);
if (!$con) exit(json_encode(array("code" => CODE_CONNECT_FAILED)));

$hasher = new PasswordHash(8,false);

$sql = "SELECT * FROM `".TABLE_USER."` where userName = '$param_username'";
$resultSet = mysqli_query($con,$sql);
if (mysqli_num_rows($resultSet) <= 0) {
    exit(json_encode(array("code" => CODE_LOGIN_USERNAME_NOTEXIST)));
}
while ($row = $resultSet->fetch_assoc()) {
    $dbPwd = $row['userPass'];
    exit($hasher->CheckPassword($param_pwd,$dbPwd) ? json_encode(array("code" => CODE_LOGIN_SUCCESS)) : json_encode(array("code" => CODE_LOGIN_FAILED)));
}