<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>我的世界插件资源站</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.staticfile.org/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <link rel="stylesheet" href="css/iconfont.css">
    <script src="js/iconfont.js"></script>
    <link rel="stylesheet" href="css/index.css">
</head>

<body>
    <div class="container">
        <header>
            <div class="head_content">
                <div class="head_menu">
                    <a href="#"><span class="iconfont icon-daohang col-xs-3 col-md-3 col-sm-3"></span></a>
                    <a href="#"><span class="iconfont icon-yonghu col-xs-5 col-md-5 col-sm-5" style="font-size: 30px;"></span></a>
                </div>
                <div class="head_logo col-lg-6 col-md-8 col-sm-8 col-xs-3">
                    <img src="image/123.png" alt="logo">
                </div>
                <div class="head_right col-lg-5 col-md-4 col-sm-5 col-xs-11">
                    <nav>
                        <div><a href="#">登录</a></div>
                        <div><a href="#">注册</a></div>
                    </nav>
                </div>
            </div>
        </header>
        <div style="clear: both;"></div>
        <main>
            <div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="content_left col-lg-5 col-sm-5 col-xs-5 col-md-5">
                    <div class="content_title col-lg-12">
                        <div style="content_rank_message">
                            <h7>热门插件</h7>
                        </div>
                    </div>
                    <!-- 排行 -->
                    <div class="plugin_rank col-lg-12">
                        <!-- 排行例子  -->
                        <a href="#">
                            <div class="rank_msg col-lg-12">
                                <div class="rank_img col-lg-2">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt=""
                                        width="50px" height="50px" style="float: left;">
                                        <div class="rank_message">
                                        <span class="col-xs-3 col-sm-3">插件名</span>
                                        <span class="col-xs-3 col-sm-3" style="font-size: 12px;">1111111</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- 排行 -->
                    <div class="plugin_rank col-lg-12">
                        <!-- 排行例子  -->
                        <a href="#">
                            <div class="rank_msg col-lg-12">
                                <div class="rank_img col-lg-2">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt=""
                                        width="50px" height="50px" style="float: left;">
                                        <div class="rank_message">
                                        <span class="col-xs-3 col-sm-3">插件名</span>
                                        <span class="col-xs-3 col-sm-3" style="font-size: 12px;">1111111</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- 排行 -->
                    <div class="plugin_rank col-lg-12">
                        <!-- 排行例子  -->
                        <a href="#">
                            <div class="rank_msg col-lg-12">
                                <div class="rank_img col-lg-2">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt=""
                                        width="50px" height="50px" style="float: left;">
                                        <div class="rank_message">
                                        <span class="col-xs-3 col-sm-3">插件名</span>
                                        <span class="col-xs-3 col-sm-3" style="font-size: 12px;">1111111</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- 排行 -->
                    <div class="plugin_rank col-lg-12">
                        <!-- 排行例子  -->
                        <a href="#">
                            <div class="rank_msg col-lg-12">
                                <div class="rank_img col-lg-2">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt=""
                                        width="50px" height="50px" style="float: left;">
                                        <div class="rank_message">
                                        <span class="col-xs-3 col-sm-3">插件名</span>
                                        <span class="col-xs-3 col-sm-3" style="font-size: 12px;">1111111</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="content_right col-lg-6">
                    <div class="plugin_data_list col-lg-12 col-md-12 col-sm-12 col-xs-12" style="font-size: 14px;">
                        <div class="dataMsg  col-lg-7 col-md-7 col-">
                            <span>插件信息</span>
                        </div>
                        <div class="data_check col-lg-5">
                            <span>查看</span>
                        </div>
                    </div>
                    <div class="plugin_res col-lg-12">
                        <!--  资源  -->
                        <div class="res_list col-lg-12">
                            <div class="res_list_content col-lg-12">
                                <div class="res_list_img col-lg-2" style="float: left;">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt="">

                                </div>
                                <div class="res_main_msg col-lg-6">
                                    <div class="res_list_title ">
                                        <div class="res_list_title col-lg-12">
                                            <span>一款非常棒的插件</span>
                                            <br>
                                            <span><a href="#">Marinda</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="res_check_main col-lg-4">
                                    <div class="res_check_title">
                                        <span>1000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  资源  -->
                        <div class="res_list col-lg-12">
                            <div class="res_list_content col-lg-12">
                                <div class="res_list_img col-lg-2" style="float: left;">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt="">
                                </div>
                                <div class="res_main_msg col-lg-6">
                                    <div class="res_list_title ">
                                        <div class="res_list_title col-lg-12">
                                            <span>一款非常棒的插件</span>
                                            <br>
                                            <span><a href="#">Marinda</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="res_check_main col-lg-4">
                                    <div class="res_check_title">
                                        <span>1000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  资源  -->
                        <div class="res_list col-lg-12">
                            <div class="res_list_content col-lg-12">
                                <div class="res_list_img col-lg-2" style="float: left;">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt="">
                                </div>
                                <div class="res_main_msg col-lg-6">
                                    <div class="res_list_title ">
                                        <div class="res_list_title col-lg-12">
                                            <span>一款非常棒的插件</span>
                                            <br>
                                            <span><a href="#">Marinda</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="res_check_main col-lg-4">
                                    <div class="res_check_title">
                                        <span>1000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  资源  -->
                        <div class="res_list col-lg-12">
                            <div class="res_list_content col-lg-12">
                                <div class="res_list_img col-lg-2" style="float: left;">
                                    <img src="https://attachment.mcbbs.net/group/fe/group_1330_icon.png" alt="">
                                </div>
                                <div class="res_main_msg col-lg-6">
                                    <div class="res_list_title ">
                                        <div class="res_list_title col-lg-12">
                                            <span>一款非常棒的插件</span>
                                            <br>
                                            <span><a href="#">Marinda</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="res_check_main col-lg-4">
                                    <div class="res_check_title">
                                        <span>1000</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu_list col-lg-12">
                        <div class="col-lg-5 col-sm-1 col-xs-1 col-md-1">
                            <a href="#" class="col-lg-12 col-sm-1 col-xs-1 col-md-1"><span>回到首页</span></a>
                        </div>
                        <div class="col-lg-1">
                            <a href="#" class="col-lg-1">
                                <span> 1</span></a></div>
                        <div class="col-lg-1">
                            <a href="#" class="col-lg-1"><span>2</span></a></div>
                        <div class="col-lg-1">
                            <a href="#" class="col-lg-1"><span>3</span></a></div>
                        <div class="col-lg-1">
                            <a href="#" class="col-lg-1"><span>4</span></a></div>
                        <div class="col-lg-1"><a href="#" class="col-lg-1"><span>5</span></a></div>
                    </div>
                </div>
            </div>
    </div>
    </main>
    <footer class="col-lg-12">
        <div class="bottom col-lg-12">
            <div class="bottom_left col-lg-6 col-sm-4 col-xs-4 col-md-4">
                <div class="bottom_friends col-lg-6 col-sm-4 col-xs-4 col-md-4">
                    <h6>友情链接</h6>
                    <p><a href="#">待添加</a></p>
                    <p><a href="#">待添加</a></p>
                </div>
            </div>
            <div class="bottom_right col-lg-6">
                <div class="bottom_author col-lg-6">
                    <p>Author: Marinda,LovelyCatV</p>
                    <p><a href="#">gitee</a> | <a href="#">github</a></p>
                </div>
            </div>
        </div>
    </footer>
    </div>
</body>

</html>