<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>注册</title>
    <link rel="stylesheet" href="css/iconfont2.css">
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="js/iconfont2.js"></script>
    <link rel="stylesheet" href="css/submit.css">
</head>
<body>
    <div class="container_fluid">
    <!-- 头部  -->
    <main>
        <div class="bg"></div>
        <div class="content">
            <div class="content_title">
                <h2 class="content_title_msg">登录</h2>
            </div>
            <div class="reg_login_change">
                <span class="iconfont icon-qiehuan"></span>
            </div>
            <div class="user_content">
                <div class="user_name">
                    <label for="uName">账号：<input type="text" placeholder="请输入你的用户名" id = "uName"></label>
                </div>
                <div class="user_pass">
                    <label for="uPass">密码：<input type="text" placeholder="请输入你的密码" id = "uPass"></label>
                </div>
                <div class="user_submit">
                    <input type="submit" id = "submit" value="登录">
                </div>
            </div>
            <div class="user_reg_content" style="display: none;">
                <div class="user_reg_name">
                    <label for="uRegName">注册账号：<input type="text" placeholder="请输入你的用户名" id = "uRegName"></label>
                </div>
                <div class="user_reg_pass">
                    <label for="uRegPass">注册密码：<input type="text" placeholder="请输入你的密码" id = "uRegPass"></label>
                </div>
                <div class="user_reg_email">
                    <label for="uRegEmail">邮箱地址：<input type="text" placeholder="请输入你的密码" id = "uRegEmail"></label>
                </div>
                <div class="user_reg_code">
                    <label for="uRegCode">验证码：<input type="text" placeholder="验证码" id = "uRegCode"></label>
                    <button class="uRegCode_btn">发送</button>
                </div>
                <div class="user_reg_submit">
                    <input type="submit" id = "submit" value="登录">
                </div>
            </div>
        </div>
    </main>
    </div>
</body>
<script type="text/javascript">
$(function(){
    var bol = true;
    $(".reg_login_change span").on('click',function(){
        if(bol){
            $(".user_content").hide(200);
            bol = false;
            $(".user_reg_content").css("display","block");
            $(".content_title h2").text("注册");
            $(".user_reg_submit input").val("注册");
        }else{
            $(".user_reg_content").css("display","none");
            $(".user_content").show(200);
            bol = true;
            $(".content_title h2").text("登录");
            $(".user_reg_submit input").val("登录");
        }
        console.log(bol);
        
    })
})
</script>
</html>